import React, { useState } from 'react'
import "./TicTacToe.css"
import tictactoe from "../assets/TicTacToe.png"


const TicTacToe = () => {
    const [turn, setTurn] = useState("X");
    const [cells, setCells] = useState(Array(9).fill(""));
    const [winner, setWinner] = useState("");
    const checkForWinner = (squares) => {
        let combos = {
            across: [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
            ],
            down: [
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
            ],
            diagnal: [
                [0, 4, 8],
                [6, 4, 2],
            ]
        };
        for (let combo in combos) {
            combos[combo].forEach((pattern) => {
                console.log(pattern)
                if(
                    squares[pattern[0]] === "" ||
                    squares[pattern[1]] === "" ||
                    squares[pattern[2]] === "" 
                ){

                }else if  (
                    squares[pattern[0]] ===  squares[pattern[1]] &&
                    squares[pattern[1]] ===  squares[pattern[2]]
                    
                    ){
setWinner(squares[pattern[0]])

                }
            }); 
        }
    
};
const checkIfTie = () => {
    let filled = true;
    cells.forEach((squares) => {
      if (squares === "") {
        filled = false;
      }
    });

    if (filled) {
        setCells(Array(9).fill(""));
    }
  };
const handelClick = (num) => {
    let squares = [...cells];
    if (cells[num] !== "") {
        alert("Already clickt");
        return;
    }

    if (turn === "X") {
        squares[num] = "X"
        setTurn("O");

    } else {
        squares[num] = "O"
        setTurn("X");
    }
    checkIfTie(squares);
    checkForWinner(squares)
    setCells(squares);
    


};

const handelRestart=()=>{
    setWinner(null);
    setCells(Array(9).fill(""));

}


const Cell = ({ num }) => {
    return <td onClick={() => handelClick(num)}>{cells[num]}</td>
}

return (
    <div className='container'>
        <img src={tictactoe} alt="logo"/>
         <div className="title"> Turn: {turn}</div>
<div className="box">
        <table>
          <tbody>
                <tr>
                    <Cell num={0} />
                    <Cell num={1} />
                    <Cell num={2} />
                </tr>
                <tr>
                    <Cell num={3} />
                    <Cell num={4} />
                    <Cell num={5} />
                </tr>
                <tr>
                    <Cell num={6} />
                    <Cell num={7} />
                    <Cell num={8} />
                </tr>
            </tbody>

        </table>
        {winner &&(
    <>
    <p className='title'>{winner} is the winner!</p>
					<button class="button" onClick={() => handelRestart()}>Play Again</button>
				</>
			)}
		</div>
        </div>
	);
};

export default TicTacToe
